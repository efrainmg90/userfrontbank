package com.efrainmg90.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.efrainmg90.domain.User;
import com.efrainmg90.domain.security.UserRole;
import com.efrainmg90.repository.RoleRepository;
import com.efrainmg90.service.UserService;

@Controller
public class HomeController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleRepository roleRepository;

	@RequestMapping(value={"/","/index"},method=RequestMethod.GET)
	public String index(){
		return "index";
	}
	
	@RequestMapping(value="/signup",method=RequestMethod.GET)
	public ModelAndView signup(){
		ModelAndView mav = new ModelAndView("signup");
		mav.addObject("user",new User());
		return mav;
	}
	
	@RequestMapping(value="/signup",method=RequestMethod.POST)
	public String signupPost(@ModelAttribute("user")User user,Model model){
		if(userService.checkUserExists(user.getUsername(), user.getEmail())){
			
			if(userService.chekEmailExists(user.getEmail()))
				model.addAttribute("emailExists",true);
			
			if(userService.chekUsernameExists(user.getUsername()))
				model.addAttribute("usernameExists",true);
			
			return "signup";
		}
		else{
			Set<UserRole> userRoles = new HashSet<>();
			userRoles.add(new UserRole(user,roleRepository.findByName("USER")));
			userService.createUser(user, userRoles);
			return "redirect:/";
		}
	}//end signPost
	
}
