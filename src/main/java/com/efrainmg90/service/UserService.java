package com.efrainmg90.service;

import java.util.Set;

import com.efrainmg90.domain.User;
import com.efrainmg90.domain.security.UserRole;

public interface UserService {
	
	public void save(User user);
	
	public User createUser(User user,Set<UserRole> userRoles);
	
	public User findByUsername(String username);
	
	public User findByEmail(String email);
	
	public boolean chekUsernameExists(String username);
	
	public boolean chekEmailExists(String email);
	
	public boolean checkUserExists(String username, String email);

}
