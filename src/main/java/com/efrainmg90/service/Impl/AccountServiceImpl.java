package com.efrainmg90.service.Impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.efrainmg90.domain.PrimaryAccount;
import com.efrainmg90.domain.SavingsAccount;
import com.efrainmg90.repository.PrimaryAccountRepository;
import com.efrainmg90.repository.SavingsAccountRepository;
import com.efrainmg90.service.AccountService;
import com.efrainmg90.service.UserService;

@Service
public class AccountServiceImpl implements AccountService{
	private static int nextAccountNumber = 11223145;
	
	@Autowired
	private PrimaryAccountRepository primaryAccountRepo;
	
	@Autowired
	private SavingsAccountRepository savingsAccountRepo;
	
	@Autowired
	private UserService userService;

	@Override
	public PrimaryAccount createPrimaryAcount() {
		// TODO Auto-generated method stub
		PrimaryAccount primaryAccount = new PrimaryAccount();
		primaryAccount.setAccountBalance(new BigDecimal(0.0));
		primaryAccount.setAccountNumber(accountGen());
		
		primaryAccountRepo.save(primaryAccount);
		return primaryAccountRepo.findByAccountNumber(primaryAccount.getAccountNumber());
	}

	@Override
	public SavingsAccount createSavingsAccount() {
		// TODO Auto-generated method stub
		SavingsAccount savingsAccount = new SavingsAccount();
		savingsAccount.setAccountBalance(new BigDecimal(0.0));
		savingsAccount.setAccountNumber(accountGen());
		
		savingsAccountRepo.save(savingsAccount);
		return savingsAccountRepo.findByAccountNumber(savingsAccount.getAccountNumber());
	}
	
	private int accountGen(){
		return ++nextAccountNumber;
	}

}
