package com.efrainmg90.service.Impl;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.efrainmg90.domain.User;
import com.efrainmg90.domain.security.UserRole;
import com.efrainmg90.repository.RoleRepository;
import com.efrainmg90.repository.UserRepository;
import com.efrainmg90.service.AccountService;
import com.efrainmg90.service.UserService;


@Service
public class UserServiceImpl implements UserService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	
	@Autowired 
	private UserRepository userRepo;
	
	@Autowired 
	private RoleRepository roleRepo;
	
	@Autowired
	private AccountService accountService;
	
	public void save(User user){
		userRepo.save(user);
	}
	
	@Transactional
	public User createUser(User user, Set<UserRole> userRoles){
		User localUser = userRepo.findByUsername(user.getUsername());
		if(localUser!=null)
			LOG.info("User already exist",user.getUsername());
		else{
			String encryptedPassword = passwordEncoder.encode(user.getPassword());
			user.setPassword(encryptedPassword);
			
			for(UserRole userRole : userRoles)
				roleRepo.save(userRole.getRole());
			
			user.getUserRoles().addAll(userRoles);
			user.setPrimaryAccount(accountService.createPrimaryAcount());
			user.setSavingsAccount(accountService.createSavingsAccount());
			
			localUser = userRepo.save(user);
		}//end else
		return localUser;
	}//end create User 
	
	public User findByUsername(String username){
		return userRepo.findByUsername(username);
	}
	
	public User findByEmail(String email){
		return userRepo.findByEmail(email);
	}
	
	public boolean chekUsernameExists(String username){
		if(findByUsername(username) != null)
			return true;
		else
			return false;	
		}
	
	public boolean chekEmailExists(String email){
		if(findByEmail(email) != null)
			return true;
		else
			return false;	
		}
	
	public boolean checkUserExists(String username, String email){
		if (chekUsernameExists(username) || chekEmailExists(email))
			return true;
		else
			return false;
	}
	
}
