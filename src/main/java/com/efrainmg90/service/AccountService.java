package com.efrainmg90.service;

import com.efrainmg90.domain.PrimaryAccount;
import com.efrainmg90.domain.SavingsAccount;

public interface AccountService {

	PrimaryAccount createPrimaryAcount();
	SavingsAccount createSavingsAccount();
}
