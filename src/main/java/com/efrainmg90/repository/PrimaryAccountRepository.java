package com.efrainmg90.repository;

import org.springframework.data.repository.CrudRepository;

import com.efrainmg90.domain.PrimaryAccount;

public interface PrimaryAccountRepository extends CrudRepository<PrimaryAccount,Long> {
	public PrimaryAccount findByAccountNumber(int accountNumber);

}
