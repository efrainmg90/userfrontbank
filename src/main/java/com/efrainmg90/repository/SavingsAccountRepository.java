package com.efrainmg90.repository;

import org.springframework.data.repository.CrudRepository;

import com.efrainmg90.domain.SavingsAccount;

public interface SavingsAccountRepository extends CrudRepository<SavingsAccount, Long>{

	public SavingsAccount findByAccountNumber(int accountNumber);
}
