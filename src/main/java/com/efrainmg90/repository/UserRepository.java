package com.efrainmg90.repository;


import org.springframework.data.repository.CrudRepository;

import com.efrainmg90.domain.User;


public interface UserRepository extends CrudRepository<User, Long>{

	public User findByUsername(String username);
	public User findByEmail(String email);
}
