package com.efrainmg90.repository;

import org.springframework.data.repository.CrudRepository;

import com.efrainmg90.domain.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{
	Role findByName(String name);
}
